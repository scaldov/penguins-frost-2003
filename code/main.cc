#include <omp.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <endian.h>
#include <stdbool.h>
#include <inttypes.h>

#include <string>
#include <iostream>
#include <filesystem>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>

#include "engine/swap.h"

#include "fileutil.hh"
#include "model2d.hh"

#define F_GREY    "\033[30m"
#define B_GREY    "\033[40m"
#define F_RED     "\033[31m"
#define B_RED     "\033[41m"
#define F_GREEN   "\033[32m"
#define B_GREEN   "\033[42m"
#define F_YELLOW  "\033[33m"
#define B_YELLOW  "\033[43m"
#define F_BLUE    "\033[34m"
#define B_BLUE    "\033[44m"
#define F_MAGENTA "\033[35m"
#define B_MAGENTA "\033[45m"
#define F_CYAN    "\033[36m"
#define B_CYAN    "\033[46m"
#define F_WHITE   "\033[37m"
#define B_WHITE   "\033[47m"
#define F_RESET   "\033[39m"
#define B_RESET   "\033[49m"

////// MEMORY ////// MEMORY ////// MEMORY //////

struct model
{
    uint32_t p_cnt;
    uint32_t t_cnt;

    struct p *ps;
    struct t *ts;
};

typedef struct Flake
{
    glm::fvec2 center;
    glm::fvec2 center_speed;
    float corner;
    float corner_speed;
    glm::fvec3 color;
    float scale;
    model2d *model;
} Flake;

GLFWmonitor* monitor;
const GLFWvidmode* mode;
GLFWwindow *window = NULL;
int run;

void quit()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        run = 0;
    }
}

float g_vp_w = 100.0, g_vp_h = 100.0;

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
    g_vp_w = width;
    g_vp_h = height;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-g_vp_w/g_vp_h, g_vp_w/g_vp_h, -1., 1., -1., 1.);
    glMatrixMode(GL_MODELVIEW);
}

int transform(glm::fvec3 *vectors, struct Flake *flake, float z)
{
    glm::mat3 model = glm::translate(glm::mat3(1.0f), glm::fvec2(flake->center));
    model = glm::rotate(model, flake->corner);
    model = glm::scale(model, glm::fvec2(flake->scale));
    auto vertex = flake->model->vertex;
    for(auto i = 0; i < flake->model->trianglei.size(); i++)
    {
        auto &triv = flake->model->trianglei[i].v;
        vectors[i * 4 + 3] = flake->color + glm::fvec3(i&7, i&7, i&7)*.05f;

        glm::fvec3 p1(vertex[triv[0]], 1.f);
        glm::fvec3 p2(vertex[triv[1]], 1.f);
        glm::fvec3 p3(vertex[triv[2]], 1.f);

        p1 = model * p1;
        p2 = model * p2;
        p3 = model * p3;
        p1.z = z;
        p2.z = z;
        p3.z = z;
        vectors[i * 4 + 0] = p1;
        vectors[i * 4 + 1] = p2;
        vectors[i * 4 + 2] = p3;
    }
    return flake->model->trianglei.size();
}

void draw_all(int cnt, glm::fvec3 *vectors)
{
    glBegin(GL_TRIANGLES);

    for(int i = 0; i < cnt; i++)
    {
        glColor3fv((const GLfloat*)&vectors[i * 4 + 3]);
        glVertex2fv((const GLfloat*)&vectors[i * 4 + 0]);
        glVertex2fv((const GLfloat*)&vectors[i * 4 + 1]);
        glVertex2fv((const GLfloat*)&vectors[i * 4 + 2]);
    }
    glEnd();
}

int randint(int n)
{
    return rand() % n;
}

void random_1(struct Flake *flakes, int i, model2d *models, int models_cnt, int clr)
{
    if(clr == 0)
    {
        flakes[i].color = glm::fvec3(.8);
    }
    if(clr == 1)
    {
        flakes[i].color = glm::fvec3(randint(1000) / 1000.0, randint(1000) / 1000.0, randint(1000) / 1000.0);
    }
    if(clr == 2)
    {
        flakes[i].color = glm::fvec3(0., (randint(51) + 204) * 0.004, 1.);
    }

    flakes[i].center.x = (randint(1000) - 500.0) / 500.0 / g_vp_h * g_vp_w;
    flakes[i].center.y = 1.1;
    flakes[i].center_speed.x = 0.0;
    flakes[i].center_speed.y = -0.0005 * (randint(900) + 100);
    flakes[i].corner = 0.0;
    flakes[i].corner_speed = (randint(628) - 314) / M_PI * 0.018;

    flakes[i].scale = (randint(5) + 5) * 0.02;

    switch(randint(models_cnt*100))
    {
    case 0 ... 99:
        flakes[i].model = &models[0];
        break;
    case 100 ... 199:
        flakes[i].model = &models[1];
        break;
    case 200 ... 299:
        flakes[i].model = &models[2];
        break;
    }
}

std::string path(std::string const &bin , std::string const &path)
{
    namespace fs = std::filesystem;
    fs::path result, pbin;
    if(bin[0] == '/') {
        pbin = fs::path(bin);
        std::cout <<"/"<< pbin << std::endl;
        result = pbin.parent_path().parent_path();
    } else {
        char cwd[1024];
        getcwd(cwd, 1024);
        pbin = fs::path(cwd);
        std::cout <<"cwd"<< pbin << std::endl;
        result = pbin.parent_path();
        result = pbin;
    }
    std::cout << result << std::endl;
    result.append(path);
    std::cout << result << std::endl;
    return result;
}

double diff_timespec(const struct timespec *time1, const struct timespec *time0)
{
    return (time1->tv_sec - time0->tv_sec) + (time1->tv_nsec - time0->tv_nsec) / 1000000000.0;
}

//using indexer_t = decltype( []{ static int i; return ++i - 1; } );

int main(int argc, char **argv)
{
    decltype(path("", "")) filepath[16];
//    filepath[indexer_t{}()] = path(argv[0], "data/model/sf_1.stl");
    int files_cnt = 0;
    filepath[files_cnt++] = path(argv[0], "data/model/sf_1.stl");
    filepath[files_cnt++] = path(argv[0], "data/model/sf_2.stl");
    filepath[files_cnt++] = path(argv[0], "data/model/sf_3.stl");
//    int files_cnt = indexer_t{}();
    auto filebuf = new FileBuffer[files_cnt];
    auto models = new model2d[files_cnt];
    for(int i = 0; i < files_cnt; i++) printf("Path %d: %s\n", i, filepath[i].c_str());
    for(int i = 0; i < files_cnt; i++){
        filebuf[i] = FileBuffer(filepath[i]);
        filebuf[i].load();
    }
    int total_tris = 0;
    for(int i = 0; i < files_cnt; i++){
        models[i].loadSTL(filebuf[i].buffer);
        total_tris += models[i].trianglei.size();
        printf("Triangles: %d\n", models[i].trianglei.size());
    }

    int cnt;
    int clr;

    if(argc < 3)
    {
        cnt = 1;
        clr = 1;
    }
    else
    {
        cnt = strtol(argv[1], NULL, 10);
        clr = strtol(argv[2], NULL, 10);
    }

    srand(time(NULL));
    printf("omp_get_max_threads() = %d\n", omp_get_max_threads());
    printf("%d\n", total_tris);
    auto model_vectors = new glm::fvec3[(4 * sizeof(glm::fvec3) * ((total_tris+0)&~0))*cnt];
    auto flakes = new Flake[cnt];

    glfwInit();

    monitor = glfwGetPrimaryMonitor();
    mode = glfwGetVideoMode(monitor);

    g_vp_h = mode->height;
    g_vp_w = mode->width;

    window = glfwCreateWindow(mode->width, mode->height, "Penguins Frost", monitor, NULL);
    //window = glfwCreateWindow(1024, 768, "Penguins Frost", NULL, NULL);
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwShowWindow(window);

    for(int i = 0; i < cnt; i++)
    {
        random_1(flakes, i, models, 3, clr);//COUNT
    }

    run = 1;

    struct timespec head = {0, 0};
    struct timespec tail = {0, 0};
    struct timespec diff = {0, 0};
    struct timespec work = {0, 1000000000 / 60};
    double dt = 0;

    //create triangle pool for threads
    //count total triangles in all objects
    total_tris = 0;
    for(int i = 0; i < cnt; i++)
    {
        total_tris += flakes[i].model->trianglei.size();
    }
    printf("%d triangles in %d models\n", total_tris, cnt);
    auto triangle_pool = new std::vector<glm::fvec3>[omp_get_max_threads()];
    auto triangle_cnt = new int[omp_get_max_threads()];
    for(int i = 0; i < omp_get_max_threads(); i ++) {
        triangle_pool[i].resize(total_tris * 4);
    }

    while(run)
    {
        clock_gettime(CLOCK_REALTIME, &head);

        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glLineWidth(2.0);
        glPointSize(4.0);
#pragma omp parallel for
        for(int i = 0; i < cnt; i++)
        {
            if(flakes[i].center.y < -1.1)
            {
                random_1(flakes, i, models, files_cnt, clr);//COUNT
            }
            else
            {
                flakes[i].center.x += flakes[i].center_speed.x * dt;
                flakes[i].center.y += flakes[i].center_speed.y * dt;
                flakes[i].corner   += flakes[i].corner_speed   * dt;
            }
        }
        for(int i = 0; i < omp_get_max_threads(); i ++) {
            triangle_cnt[i] = 0;
        }
        float r_cnt = 1.0f / cnt;
#pragma omp parallel for
        for(int i = 0; i < cnt; i++)
        {
            int k = omp_get_thread_num();
            int ntris = triangle_cnt[k];
            //ntris += transform(&triangle_pool[k][ntris * 4], &flakes[i], (float)i * r_cnt);
            ntris += transform(&triangle_pool[k][ntris * 4], &flakes[i], r_cnt * i);
            triangle_cnt[k] = ntris;
        }
        for(int i = 0; i < omp_get_max_threads(); i ++) {
            draw_all(triangle_cnt[i], &triangle_pool[i][0]);
        }
//        glBegin(GL_LINES);
//        glVertex3f(-1.0, 1.0, -0.9);
//        glVertex3f(-1.0, 1.0, -0.9);
        glFlush();
        glfwSwapBuffers(window);
        glfwPollEvents();
        clock_gettime(CLOCK_REALTIME, &tail);
        dt = diff_timespec(&tail, &head);
    }

    quit();
}
