#include <omp.h>
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glut.h>
// GLFW
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <endian.h>
#include <stdbool.h>
#include <inttypes.h>

#include <string>
#include <iostream>
#include <filesystem>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>

#include "engine/swap.h"

#include "fileutil.hh"
#include "model2d.hh"

#include "shader.h"

#define F_GREY    "\033[30m"
#define B_GREY    "\033[40m"
#define F_RED     "\033[31m"
#define B_RED     "\033[41m"
#define F_GREEN   "\033[32m"
#define B_GREEN   "\033[42m"
#define F_YELLOW  "\033[33m"
#define B_YELLOW  "\033[43m"
#define F_BLUE    "\033[34m"
#define B_BLUE    "\033[44m"
#define F_MAGENTA "\033[35m"
#define B_MAGENTA "\033[45m"
#define F_CYAN    "\033[36m"
#define B_CYAN    "\033[46m"
#define F_WHITE   "\033[37m"
#define B_WHITE   "\033[47m"
#define F_RESET   "\033[39m"
#define B_RESET   "\033[49m"

struct model
{
    uint32_t p_cnt;
    uint32_t t_cnt;

    struct p *ps;
    struct t *ts;
};

class Flake
{
public:
    Flake(model2d *model):model(model){}
    Flake():model(0){}
    ~Flake(){}
    int instance_cnt = 0;
    model2d *model;
    GLuint vbo_vertex_array, vbo_color, vbo_baricenter, vbo_center, vbo_scale, vbo_phi;
    std::vector<glm::fvec3> center;
    std::vector<glm::fvec2> center_speed;
    std::vector<float> phi;
    std::vector<float> omega;
    std::vector<glm::fvec3> color;
    std::vector<float> scale;
    void Resize(int new_size) {
        instance_cnt ++;
        center.resize(new_size);
        center_speed.resize(new_size);
        phi.resize(new_size);
        omega.resize(new_size);
        color.resize(new_size);
        scale.resize(new_size);
    }
};

int g_files_cnt = 0;
double g_phi = 0., g_theta = 0., g_time = 0.;
float g_vp_w = 100.0, g_vp_h = 100.0;
static int g_frame = 0;
float g_motion = 1.0, g_time_dir = 1.0;
glm::vec3 g_eyepos, g_lightdir, g_lightpos = glm::vec3(0., 0.1, 0.5);
glm::mat4 g_projection, g_view, g_model;
gls::Program glprogram;
std::vector<Flake> flakes;

GLFWmonitor* monitor;
const GLFWvidmode* mode;
GLFWwindow *window = NULL;
int run;

void quit() {
    glfwDestroyWindow(window);
    glfwTerminate();
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        run = 0;
    }
    if(key == GLFW_KEY_SPACE && action == GLFW_PRESS)
    {
        g_motion = 0.5 - (g_motion - 0.5);
        printf("g_motion = %f\n", g_motion);
    }
    if(key == GLFW_KEY_R && action == GLFW_PRESS)
    {
        g_time_dir *= -1.;
    }
}

void cursor_callback(GLFWwindow* window, double x,double y) {
    g_lightpos.x = (x / g_vp_w * 2. - 1.) * g_vp_w/g_vp_h;
    g_lightpos.y = (g_vp_h - y) / g_vp_h * 2. - 1.;
}


void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
    g_vp_w = width;
    g_vp_h = height;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-g_vp_w/g_vp_h, g_vp_w/g_vp_h, -1., 1., -1., 1.);
    glMatrixMode(GL_MODELVIEW);
}


int randint(int n) {
    return rand() % n;
}

float randf() {
    return (float)(rand()) / (float)RAND_MAX;
}

std::string path(std::string const &bin , std::string const &path) {
    namespace fs = std::filesystem;
    fs::path result, pbin;
    if(bin[0] == '/') {
        pbin = fs::path(bin);
//        std::cout <<"/"<< pbin << std::endl;
        result = pbin.parent_path().parent_path();
    } else {
        char cwd[1024];
        getcwd(cwd, 1024);
        pbin = fs::path(cwd);
//        std::cout <<"cwd"<< pbin << std::endl;
        result = pbin.parent_path();
        result = pbin;
    }
//    std::cout << result << std::endl;
    result.append(path);
//    std::cout << result << std::endl;
    return result;
}

glm::fvec3 maximize(const glm::fvec3 &v) {
    float m = glm::max(glm::max(v.x, v.y), v.z);
    float f = 1.0 / m;
    return v * f;
}

std::vector<Flake>& GenerateFlakes(int cnt, model2d *models, int models_cnt, int clr)
{
    std::vector<Flake> &flake = *(new std::vector<Flake>(models_cnt));
    for(int i = 0; i < models_cnt; i ++) flake[i].model = &models[i];
    for(int i = 0; i < cnt; i ++) {
        int model_index = randint(1000) % models_cnt;
        auto n = flake[model_index].center.size();
        flake[model_index].Resize(n + 1);
        flake[model_index].center[n] = {(randf() - 0.5) * 2. / g_vp_h * g_vp_w, 1.1f, (randf() - 0.5) * 2.};
        flake[model_index].center_speed[n] = {0.0f, -0.0005 * (randf() * 800. + 100)};
        flake[model_index].omega[n] = (randf() - 0.5) * 2. * M_PI * .5;
        flake[model_index].scale[n] = randf() * 2. * 0.025 + 0.05;
        glm::fvec3 color;
        if(clr == 0) {
            color = glm::fvec3(.8);
        }
        if(clr == 1) {
            color = glm::fvec3(randf(), randf(), randf());
        }
        if(clr == 2) {
            color = glm::fvec3(0., (randf() * 51. + 204) * 0.004, 1.);
        }
        flake[model_index].color[n] = maximize(color);
    }
    return flake;
}

//using indexer_t = decltype( []{ static int i; return ++i - 1; } );

void process_physics() {
    auto static t_0 = std::chrono::steady_clock::now();
    auto static t_1 = std::chrono::steady_clock::now();
    t_1 = std::chrono::steady_clock::now();
    float dt = std::chrono::duration<double>(t_1 - t_0).count();
    dt *= g_motion * g_time_dir;
    g_time += dt;
//#pragma omp parallel
    for(int j = 0; j < g_files_cnt; j++){
        int n = flakes[j].center.size();
//#pragma omp parallel for
        for(int i = 0; i < n; i++) {
            if(flakes[j].center[i].y < -1.1) {
                flakes[j].center[i] = {(randint(1000) - 500.0) / 500.0 / g_vp_h * g_vp_w, 1.1f, (randint(1000) - 500.0) / 500.0};
            } else {
                auto v = glm::vec3(flakes[j].center_speed[i], 0.0f);
                flakes[j].center[i] = flakes[j].center[i] + v * (float)dt;
                flakes[j].phi[i] = flakes[j].phi[i] + flakes[j].omega[i] * (float)dt;
            }
        }
    }
    t_0 = t_1;
}


void draw_flake(const Flake &flake)
{
    GLint gll = glprogram.GetUniformLocation("projection");
    glUniformMatrix4fv(gll, 1, 0, &g_projection[0][0]);
    gll = glprogram.GetUniformLocation("view");
    glUniformMatrix4fv(gll, 1, 0, &g_view[0][0]);
    gll = glprogram.GetUniformLocation("model");
    glUniformMatrix4fv(gll, 1, 0, &g_model[0][0]);
    gll = glprogram.GetUniformLocation("time");
    glUniform1f(gll, g_time);
    gll = glprogram.GetUniformLocation("lightdir");
    glUniform3fv(gll, 1, &g_lightdir.x);
    gll = glprogram.GetUniformLocation("light_pos");
    glUniform3fv(gll, 1, &g_lightpos.x);
    gll = glprogram.GetUniformLocation("eyepos");
    glUniform3fv(gll, 1, &g_eyepos.x);
    glprogram.Use();
    glBindBuffer(GL_ARRAY_BUFFER, flake.vbo_vertex_array);
    gll = glprogram.GetAttribLocation("in_pos");
    glVertexAttribPointer(gll, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), (void*) 0);
    glEnableVertexAttribArray(gll);
    glVertexAttribDivisor(gll, 0);    // 1 baricenter per 3 triangle points

    glBindBuffer(GL_ARRAY_BUFFER, flake.vbo_baricenter);
    gll = glprogram.GetAttribLocation("in_baricenter");
    glVertexAttribPointer(gll, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(gll);
    glVertexAttribDivisor(gll, 0);    // 1 baricenter per 3 triangle points

    glBindBuffer(GL_ARRAY_BUFFER, flake.vbo_color);
    gll = glprogram.GetAttribLocation("in_color");
    glVertexAttribPointer(gll, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(gll);
    glVertexAttribDivisor(gll, 1);    // 1 color per 3 triangle points

    glBindBuffer(GL_ARRAY_BUFFER, flake.vbo_center);
    gll = glprogram.GetAttribLocation("in_center");
    glVertexAttribPointer(gll, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(gll);
    glVertexAttribDivisor(gll, 1);    // 1 color per 3 triangle points

    glBindBuffer(GL_ARRAY_BUFFER, flake.vbo_scale);
    gll = glprogram.GetAttribLocation("in_scale");
    glVertexAttribPointer(gll, 1, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(gll);
    glVertexAttribDivisor(gll, 1);    // 1 color per 3 triangle points

    glBindBuffer(GL_ARRAY_BUFFER, flake.vbo_phi);
    gll = glprogram.GetAttribLocation("in_phi");
    glVertexAttribPointer(gll, 1, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(gll);
    glVertexAttribDivisor(gll, 1);    // 1 color per 3 triangle points
    //    glEnable(GL_CULL_FACE);
    glDisable(GL_CULL_FACE);
    //    glCullFace(GL_FRONT);
    //    glCullFace(GL_BACK);
    //    glCullFace(GL_FRONT_AND_BACK);
    glPointSize(2.f);
    glDrawArraysInstanced(GL_TRIANGLES, 0, flake.model->triangle.size() * 3, flake.instance_cnt);
//    glDrawArraysInstanced(GL_POINTS, 0, flake.model->triangle.size() * 3, flake.instance_cnt);
    glDisableVertexAttribArray(0);
//    printf("x,y=%f, %f\n", flake.center[0].x, flake.center[0].y);
//    for(auto &center: flake.center) printf("%g, %g\n", center.x, center.y);
//    for(auto &phi: flake.phi) printf("%g\n", phi);
//    for(auto &o: flake.omega) printf("%g\n", o);
//    printf("inst=%d\n", flake.instance_cnt);
//    for(auto &bc: flake.color) printf("%g, %g\n", bc.x, bc.y);
//    for(auto &s: flake.scale) printf("%f\n", s);
//    printf("size=%d\n", flakes.size());
}

void set_scene() {
    glm::vec2 v = {0., 1.};
    glm::mat4 mvp, mv;
    g_lightdir = {0.0, 0.0, 1.0};
    //compute MPV matrix
    g_eyepos = glm::vec3(0., 0., 1.);
    g_projection = glm::ortho(-g_vp_w/g_vp_h, g_vp_w/g_vp_h, -1.f, 1.f, -1.f, 1.f);
    g_view       = glm::lookAt(
                g_eyepos, //cam world coords
                glm::vec3(0,0,0), //cam towards to
                glm::vec3(0,1,0)  //north pole vector
                );
    g_model = glm::rotate(glm::mat4(1.0), (float)g_phi, glm::vec3(.0, .0, 1.0));
    g_model = glm::rotate(g_model, (float)g_theta, glm::vec3(0.0, 1.0, 0.0));
    //
    glClearColor(0, 0, 0, 1);
    glEnable(GL_DEPTH_TEST);
//    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    g_phi += 0.01;
    g_theta += 0.003;
//    g_time += 0.1;
    printf("f = %d\n", g_frame);
    g_frame += 1;
}

void draw_scene() {
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glLineWidth(2.0);
    glPointSize(4.0);
    set_scene();
    for(auto &f: flakes) {
        glBindBuffer(GL_ARRAY_BUFFER, f.vbo_center);
        glBufferData(GL_ARRAY_BUFFER, f.center.size() * sizeof(f.center[0]), f.center.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, f.vbo_phi);
        glBufferData(GL_ARRAY_BUFFER, f.phi.size() * sizeof(f.phi[0]), f.phi.data(), GL_STATIC_DRAW);
    }
    for(auto &f: flakes) {
        draw_flake(f);
    }
    glFlush();
    glfwSwapBuffers(window);
    glfwPollEvents();
}

void create_buffers(){
    for(auto &f: flakes) {
        glGenBuffers(1, &f.vbo_vertex_array);
        glBindBuffer(GL_ARRAY_BUFFER, f.vbo_vertex_array);
        glBufferData(GL_ARRAY_BUFFER, f.model->triangle.size() * sizeof(f.model->triangle[0]), f.model->triangle.data(), GL_STATIC_DRAW);
        glGenBuffers(1, &f.vbo_baricenter);
        glBindBuffer(GL_ARRAY_BUFFER, f.vbo_baricenter);
        glBufferData(GL_ARRAY_BUFFER, f.model->baricenter.size() * sizeof(f.model->baricenter[0]), f.model->baricenter.data(), GL_STATIC_DRAW);
        glGenBuffers(1, &f.vbo_color);
        glBindBuffer(GL_ARRAY_BUFFER, f.vbo_color);
        glBufferData(GL_ARRAY_BUFFER, f.color.size() * sizeof(f.color[0]), f.color.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glGenBuffers(1, &f.vbo_center);
        glBindBuffer(GL_ARRAY_BUFFER, f.vbo_center);
        glBufferData(GL_ARRAY_BUFFER, f.center.size() * sizeof(f.center[0]), f.center.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glGenBuffers(1, &f.vbo_scale);
        glBindBuffer(GL_ARRAY_BUFFER, f.vbo_scale);
        glBufferData(GL_ARRAY_BUFFER, f.scale.size() * sizeof(f.scale[0]), f.scale.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glGenBuffers(1, &f.vbo_phi);
        glBindBuffer(GL_ARRAY_BUFFER, f.vbo_phi);
        glBufferData(GL_ARRAY_BUFFER, f.phi.size() * sizeof(f.phi[0]), f.phi.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}

int main(int argc, char **argv)
{
    decltype(path("", "")) filepath[16];
//    filepath[indexer_t{}()] = path(argv[0], "data/model/sf_1.stl");
    filepath[g_files_cnt++] = path(argv[0], "data/model/sf_1.stl");
    filepath[g_files_cnt++] = path(argv[0], "data/model/sf_2.stl");
    filepath[g_files_cnt++] = path(argv[0], "data/model/sf_3.stl");
//    int files_cnt = indexer_t{}();
    auto filebuf = new FileBuffer[g_files_cnt];
    auto models = new model2d[g_files_cnt];
    for(int i = 0; i < g_files_cnt; i++) printf("Path %d: %s\n", i, filepath[i].c_str());
    for(int i = 0; i < g_files_cnt; i++){
        filebuf[i] = FileBuffer(filepath[i]);
        filebuf[i].load();
    }
    int total_tris = 0;
    for(int i = 0; i < g_files_cnt; i++){
        models[i].loadSTL(filebuf[i].buffer);
        total_tris += models[i].trianglei.size();
        printf("Triangles: %d\n", models[i].trianglei.size());
    }

    int cnt;
    int clr;

    if(argc < 3)
    {
        cnt = 1;
        clr = 1;
    }
    else
    {
        cnt = strtol(argv[1], NULL, 10);
        clr = strtol(argv[2], NULL, 10);
    }

    srand(time(NULL));
    printf("omp_get_max_threads() = %d\n", omp_get_max_threads());
    printf("%d\n", total_tris);

    glfwInit();

    int monitor_cnt;
    auto monitors = glfwGetMonitors(&monitor_cnt);
    monitor = glfwGetPrimaryMonitor();
    monitor = monitors[0];
    mode = glfwGetVideoMode(monitor);

    g_vp_h = mode->height;
    g_vp_w = mode->width;

    window = glfwCreateWindow(mode->width, mode->height, "Penguins Frost", monitor, NULL);
    //window = glfwCreateWindow(1024, 768, "Penguins Frost", NULL, NULL);
    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    glewInit();

    glprogram.Attach(gls::VertexShader(path(argv[0],"data/shader/shader.v")));
    glprogram.Attach(gls::FragmentShader(path(argv[0],"data/shader/shader.f")));
    glprogram.Link();

    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, cursor_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwShowWindow(window);

    flakes =  GenerateFlakes(cnt, models, g_files_cnt, clr);

    run = 1;

    //create triangle pool for threads
    //count total triangles in all objects
    printf("%d triangles in %d models\n", total_tris, cnt);
    create_buffers();
    while(run)
    {
        process_physics();
        draw_scene();
    }
    quit();
}
