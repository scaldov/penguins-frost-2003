#pragma once

#include <stdint.h>
#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <filesystem>

#include <GL/glew.h>

#include <murmur3.hh>
#include "fileutil.hh"

namespace gls {

class Shader{
public:
    std::string path;
    uint32_t shader_type;
    GLuint shader;
    Shader(std::string path = "", uint32_t shader_type = 0, GLuint shader = 0):
        path(std::move(path)), shader_type(std::move(shader_type)), shader(std::move(shader)){
    }
    void Load(const std::string &path, uint32_t shader_type) {
        this->path = path;
        this->shader_type = shader_type;
        auto bfr = FileBuffer(path);
        bfr.load();
//        std::cout << bfr.buffer << std::endl;
        const GLchar* shaderCode = (GLchar*)bfr.buffer;
        // Compile shaders
        GLint success;
        GLchar infoLog[1024];
        shader = glCreateShader(shader_type);
        glShaderSource(shader, 1, &shaderCode, NULL);
        glCompileShader(shader);
        // Print compile errors if any
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(shader, sizeof(infoLog), NULL, infoLog);
            std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
        }
    }
    virtual void Load(const std::string &path) {}
};

class VertexShader: public Shader{
public:
    VertexShader(){VertexShader("");}
    VertexShader(const std::string &path){
        Load(path);
    }
    virtual void Load(const std::string &path) {
        Shader::Load(path, GL_VERTEX_SHADER);
    }
};

class FragmentShader: public Shader{
public:
    FragmentShader(){FragmentShader("");}
    FragmentShader(const std::string &path)
    {
        Load(path);
    }
    virtual void Load(const std::string &path) {
        Shader::Load(path, GL_FRAGMENT_SHADER);
    }
};


class Program
{
public:
    bool linked;
    GLuint program;
    std::vector<GLuint> shaders;
    std::map<uint32_t, int> mapLocation, mapLocationAttr;
    Program(): linked(false), program(-1) {}
    ~Program() {
        for(auto s: shaders){
            printf("shader %d destroyed\n", s);
            glDeleteShader(s);
        }
    }
    void Attach(const Shader &shader){
        if(program == -1){
            program = glCreateProgram();
        }
        printf("shader %d attached\n", shader.shader);
        shaders.push_back(shader.shader);
        glAttachShader(program, shader.shader);
        linked = false;
    }
    void Link() {
        if(!linked) {
            int success;
            GLchar infoLog[1024];
            int infoLog_len = 1024;
            glLinkProgram(program);
            // Print linking errors if any
            glGetProgramiv(program, GL_LINK_STATUS, &success);
            if (!success)
            {
                glGetProgramInfoLog(program, sizeof(infoLog), &infoLog_len, infoLog);
                printf("%s\n", infoLog);
                std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
            } else linked = true;
        }
    }
    void Use()
    {
        Link();
        glUseProgram(program);
    }
    int GetUniformLocation(const char *name) {
        if(!linked) throw std::runtime_error("get location in non-linked program");
        const uint32_t key = cx::murmur3_32(name, 0);
        auto it = mapLocation.find(key);
        if(it != mapLocation.end()) {
            //            printf("found\n");
            return it->second;
        } else {
            int gll_mvp = glGetUniformLocation (program, name);
            mapLocation[key] = gll_mvp;
            //            printf("cached\n");
            return gll_mvp;
        }
    }
    int GetAttribLocation(const char *name) {
        if(!linked) throw std::runtime_error("get location in non-linked program");
        const uint32_t key = cx::murmur3_32(name, 0);
        auto it = mapLocationAttr.find(key);
        if(it != mapLocationAttr.end()) {
            //            printf("found\n");
            return it->second;
        } else {
            int gll_mvp = glGetAttribLocation(program, name);
            mapLocationAttr[key] = gll_mvp;
            //            printf("cached\n");
            return gll_mvp;
        }
    }
};


}//end namespace
