#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <endian.h>
#include <stdbool.h>
#include <inttypes.h>

#include <string>
#include <iostream>
#include <filesystem>

size_t g_mem_total = 0;
int g_mem_chunk_cnt = 0;
const int g_mem_chunk_max = 16384;
void* g_mem_chunk_ptr[g_mem_chunk_max];
size_t g_mem_chunk_size[g_mem_chunk_max];

int mem_chunk_find(void *p){
    for(int i = 0; i < g_mem_chunk_cnt + 1; ++i){
        if(g_mem_chunk_ptr[i] == p){
            return i;
        }
    }
    return -1;
}

void* operator new (size_t size)
{
    printf("new %d bytes, %d total\n", size, g_mem_total);
    void* p;
    p = malloc(size);
    g_mem_total += size;
    int i = mem_chunk_find(0);
    if(i == -1) {
        throw std::runtime_error("chunk not found");
    }
    g_mem_chunk_ptr[i] = p;
    g_mem_chunk_size[i] = size;
    g_mem_chunk_cnt ++;
    printf("addr = %p\n", p);
    return p;
}
void * operator new[] (size_t size)
{
    printf("new %d bytes, %d total\n", size, g_mem_total);
    void* p;
    p = malloc(size);
    g_mem_total += size;
    int i = mem_chunk_find(0);
    if(i == -1) {
        throw std::runtime_error("chunk not found");
    }
    g_mem_total += size;
    g_mem_chunk_ptr[i] = p;
    g_mem_chunk_size[i] = size;
    g_mem_chunk_cnt ++;
    printf("addr = %p\n", p);
    return p;
}
void operator delete (void* p)
{
    free(p);
    int i = mem_chunk_find(p);
    if(i == -1) {
        printf("warning %p not found\n", p);
        return;
        throw std::runtime_error("chunk not found @free");
    }
    auto size = g_mem_chunk_size[i];
    g_mem_total -= size;
    g_mem_chunk_ptr[i] = 0;
    g_mem_chunk_size[i] = 0;
    g_mem_chunk_cnt --;
    printf("free %d bytes, %d total\n", size, g_mem_total);
}
void operator delete[] (void* p)
{
    free(p);
    int i = mem_chunk_find(p);
    if(i == -1) {
        printf("warning %p not found\n", p);
        return;
        throw std::runtime_error("chunk not found @free");
    }
    auto size = g_mem_chunk_size[i];
    g_mem_total -= size;
    g_mem_chunk_ptr[i] = 0;
    g_mem_chunk_size[i] = 0;
    g_mem_chunk_cnt --;
    printf("free %d bytes, %d total\n", size, g_mem_total);
}
