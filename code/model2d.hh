#pragma once
#include <vector>
#include <glm/glm.hpp>

struct TriangleI
{
    int v[3];
};

struct Triangle
{
    glm::fvec2 v[3];
};

class model2d {
public:
    glm::fvec3 color;
    std::vector<glm::vec2> vertex;
//    std::vector<glm::vec2> face;
    std::vector<TriangleI> trianglei;
//    std::vector<glm::vec2> line;
    std::vector<Triangle> triangle;
    std::vector<Triangle> baricenter;
    model2d();
    ~model2d();
    int loadSTL(void *data);
};
