#include "model2d.hh"

#include <endian.h>
#include <stdio.h>

model2d::model2d(){}

model2d::~model2d(){}

struct Poly3D
{
    glm::fvec3 v[3];
    glm::fvec3 n;
};

int vfind(std::vector<glm::vec2> &vertex, glm::vec2 v) {
    for(int i = 0; i < vertex.size(); ++i) {
        if(glm::distance(vertex[i], v) < 1e-20) return i;
    }
    return -1;
}

int vfind_append(std::vector<glm::vec2> &vertex, glm::vec2 v) {
    int iv = vfind(vertex, v);
    if(iv >= 0) return iv;
    iv = vertex.size();
    vertex.push_back(v);
    return iv;
}

int model2d::loadSTL(void *data){
    auto host_float = [](void *x) { float f; *((uint32_t*)&f) = le32toh(*((uint32_t*)x)); return f; };
    auto host_u32 = [](void *x) { return le32toh(*(uint32_t*)(x)); };
    uint8_t *p = (uint8_t *)data;
    p += 80;
    int cnt_load = host_u32(p);
    int cnt = 0;
    p += 4;
    std::vector<Poly3D> poly_tmp(cnt_load);
    using T = decltype(host_float(0));
    const int tsize = sizeof(T);
    for(int i = 0; i < cnt_load; ++i)
    {
        auto &poly = poly_tmp[i];
        poly.n = {host_float(&p[0 * tsize]), host_float(&p[1 * tsize]), host_float(&p[2 * tsize])};
        poly.v[0] = {host_float(&p[3 * tsize]), host_float(&p[4 * tsize]), host_float(&p[5 * tsize])};
        poly.v[1] = {host_float(&p[6 * tsize]), host_float(&p[7 * tsize]), host_float(&p[8 * tsize])};
        poly.v[2] = {host_float(&p[9 * tsize]), host_float(&p[10 * tsize]), host_float(&p[11 * tsize])};
        p += 12 * tsize + 2;
    }
    for(int i = 0; i < cnt_load; ++i) {
        if(poly_tmp[i].n == glm::fvec3(0., 0., 1.)){
            auto iv0 = vfind_append(vertex, poly_tmp[i].v[0]);
            auto iv1 = vfind_append(vertex, poly_tmp[i].v[1]);
            auto iv2 = vfind_append(vertex, poly_tmp[i].v[2]);
            printf("%d, %d, %d\n", iv0, iv1, iv2);
            auto baricenter = (poly_tmp[i].v[0] + poly_tmp[i].v[1] + poly_tmp[i].v[2]) / 3.0f;
            Triangle tr = {poly_tmp[i].v[0], poly_tmp[i].v[1], poly_tmp[i].v[2]};
            TriangleI ti = {iv0, iv1, iv2};
            this->baricenter.push_back({baricenter, baricenter, baricenter});
            triangle.push_back(tr);
            trianglei.push_back(ti);
        }
    }
    return trianglei.size();
}
