#pragma once
#include <filesystem>

class FileBuffer {
public:
    std::filesystem::path path;
    uint8_t *buffer;
    size_t length;
    FileBuffer();
    FileBuffer(std::filesystem::path const &path);
//    FileBuffer(std::string);
    ~FileBuffer();
    int load();
};
