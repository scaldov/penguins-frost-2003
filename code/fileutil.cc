#include "fileutil.hh"

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


FileBuffer::FileBuffer() :
    buffer(0), length(0){
}

FileBuffer::FileBuffer(std::filesystem::path const &path):
    buffer(0), length(0), path(path) {
    std::cout << "path = " << path << std::endl;
}

FileBuffer::~FileBuffer() {
    if(buffer){
        free(buffer);
    }
}

int FileBuffer::load() {
    int l = std::filesystem::file_size(path);
    if(l >= 0) buffer = new uint8_t[l + 1];
    int f = open(path.c_str(), O_RDONLY);
    int r = read(f, buffer, l);
    close(f);
    buffer[l] = 0;
    return r;
}
