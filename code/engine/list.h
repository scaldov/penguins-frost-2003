#ifndef LIST
#define LIST

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

struct node
{
    struct node *prev;
    struct node *next;
    void *data;
};

struct list
{
    struct node *head;
    struct node *tail;
    size_t to_head;
    size_t to_tail;
};

void node_create(struct node **node);
void node_delete(struct node **node);
void list_create(struct list **list);
void list_delete(struct list **list);

void list_lpush(struct list *list, void *data);
void list_rpush(struct list *list, void *data);
void *list_lpop(struct list *list);
void *list_rpop(struct list *list);
void *list_head(struct list *list);
void *list_tail(struct list *list);

#endif
