#include "list.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

int list_lpush(struct list *list, void *data) {
    assert(list != NULL);

    struct list_node *node = list_node_new(data);

    if (node == NULL) return LIST_ENOMEM;

    if (list->len == 0) {
        assert(list->head == NULL && list->tail == NULL);
        list->head = node;
        list->tail = node;
    } else {
        assert(list->head != NULL && list->tail != NULL);
        struct list_node *head = list->head;
        assert(head->prev == NULL && node->prev == NULL);
        head->prev = node;
        node->next = head;
        list->head = node;
    }

    list->len += 1;
    return LIST_OK;
}

/* Push an item to list on the right. */
int list_rpush(struct list *list, void *data) {
    assert(list != NULL);

    struct list_node *node = list_node_new(data);

    if (node == NULL) return LIST_ENOMEM;

    if (list->len == 0) {
        assert(list->head == NULL && list->tail == NULL);
        list->head = node;
        list->tail = node;
    } else {
        assert(list->head != NULL && list->tail != NULL);
        struct list_node *tail = list->tail;
        assert(tail->next == NULL && node->next == NULL);
        tail->next = node;
        node->prev = tail;
        list->tail = node;
    }

    list->len += 1;
    return LIST_OK;
}

/* Pop an item from list on the left. */
void *list_lpop(struct list *list) {
    assert(list != NULL);

    if (list->len == 0) {
        assert(list->head == NULL && list->tail == NULL);
        return NULL;
    }

    assert(list->head != NULL && list->tail != NULL);

    struct list_node *head = list->head;
    struct list_node *node = head->next;

    if (node == NULL) {
        assert(list->len == 1);
        list->tail = NULL;
    } else {
        assert(list->len >= 2);
        node->prev = NULL;
    }

    list->head = node;
    list->len -= 1;

    void *data = head->data;
    list_node_free(head);
    return data;
}

/* Pop an item from list on the right. */
void *list_rpop(struct list *list) {
    assert(list != NULL);

    if (list->len == 0) {
        assert(list->head == NULL && list->tail == NULL);
        return NULL;
    }

    assert(list->head != NULL && list->tail != NULL);

    struct list_node *tail = list->tail;
    struct list_node *node = tail->prev;

    if (node == NULL) {
        assert(list->len == 1);
        list->head = NULL;
    } else {
        assert(list->len >= 2);
        node->next = NULL;
    }

    list->tail = node;
    list->len -= 1;

    void *data = tail->data;
    list_node_free(tail);
    return data;
}

/* Get the head node data, NULL on empty list. */
void *list_head(struct list *list) {
    assert(list != NULL);

    if (list->len == 0) {
        assert(list->head == NULL && list->tail == NULL);
        return NULL;
    }

    assert(list->head != NULL && list->tail != NULL);
    return list->head->data;
}

/* Get the tail node data, NULL on empty list. */
void *list_tail(struct list *list) {
    assert(list != NULL);

    if (list->len == 0) {
        assert(list->head == NULL && list->tail == NULL);
        return NULL;
    }

    assert(list->head != NULL && list->tail != NULL);
    return list->tail->data;
}
