#include "engine.h"

//
//void test_find(aabb_t *aabb, model_t *model)
//{
//    aabb->box_head.x = FLT_MIN;
//    aabb->box_head.y = FLT_MIN;
//    aabb->box_tail.x = FLT_MAX;
//    aabb->box_tail.y = FLT_MAX;
//
//    size_t head = aabb->head;
//    size_t tail = aabb->tail;
//
//    f32vec2_t point;
//
//    while(1)
//    {
//        model_get_point(model, head, &point.x, &point.y);
//
//        aabb->box_head.x = fmax(aabb->box_head.x, point.x);
//        aabb->box_head.y = fmax(aabb->box_head.y, point.y);
//        aabb->box_tail.x = fmin(aabb->box_tail.x, point.x);
//        aabb->box_tail.y = fmin(aabb->box_tail.y, point.y);
//
//        if(head < tail)
//        {
//            head += 1;
//        } else
//        {
//            break;
//        }
//    }
//}
//
// void test_calc(aabb_t *aabb, model_t *model)
// {
//     float x = 0.0;
//     float y = 0.0;
//
//     float last_x = 0;
//     float last_y = 0;
//
//     float dx = 0.0;
//     float dy = 0.0;
//
//     bool X = false;
//     bool Y = false;
//
//     bool last_X = true;
//     bool last_Y = true;
//
//     int count = 0;
//     int level = 1;
//
//     while(1)
//     {
//         last_x = x;
//         last_y = y;
//
//         last_X = X;
//         last_Y = Y;
//
//         test_find(aabb, model);
//
//         x = aabb->box_head.x - aabb->box_tail.x;
//         y = aabb->box_head.y - aabb->box_tail.y;
//
//         dx = x - last_x;
//         dy = y - last_y;
//
//         if(dx > dy) { X = true; }
//         if(dy > dx) { Y = true; }
//
//         if((X == true) && (last_X == false) && count > level){ printf("Changed to X!\n"); break; }
//         if((Y == true) && (last_Y == false) && count > level){ printf("Changed to Y!\n"); break; }
//
//         aabb->tail += 1;
//         count += 1;
//         if(aabb->tail == model_count_points(model)) { break; }
//     }
// }

//void force_update(force_t *force, float time)
//{
//    force->corner_speed.x += force->corner_boost.x * time;
//    force->center_speed.x += force->center_boost.x * time;
//    force->center_speed.y += force->center_boost.y * time;
//    force->corner_delta.x = force->corner_speed.x * time;
//    force->center_delta.x = force->center_speed.x * time;
//    force->center_delta.y = force->center_speed.y * time;
//    force->corner.x += force->corner_delta.x;
//    force->center.x += force->center_delta.x;
//    force->center.y += force->center_delta.y;
//}
