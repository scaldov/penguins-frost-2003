#include "swap.h"

#include <stdint.h>
#include <stdlib.h>
#include <endian.h>
#include <stdbool.h>

#define SIZE_U08 sizeof(uint8_t )
#define SIZE_U16 sizeof(uint16_t)
#define SIZE_U32 sizeof(uint32_t)
#define SIZE_U64 sizeof(uint64_t)
#define SIZE_I08 sizeof(int8_t )
#define SIZE_I16 sizeof(int16_t)
#define SIZE_I32 sizeof(int32_t)
#define SIZE_I64 sizeof(int64_t)

uint16_t htobe_u16(uint16_t x) { return htobe16(x); }

uint16_t htole_u16(uint16_t x) { return htole16(x); }

uint32_t htobe_u32(uint32_t x) { return htobe32(x); }

uint32_t htole_u32(uint32_t x) { return htole32(x); }

uint64_t htobe_u64(uint64_t x) { return htobe64(x); }

uint64_t htole_u64(uint64_t x) { return htole64(x); }

void htobe_f32(float *x) { *(uint32_t*)x = htobe32(*(uint32_t*)x); }

void htole_f32(float *x) { *(uint32_t*)x = htole32(*(uint32_t*)x); }

void htobe_f64(double *x) { *(uint64_t*)x = htobe64(*(uint64_t*)x); }

void htole_f64(double *x) { *(uint64_t*)x = htole64(*(uint64_t*)x); }

uint16_t betoh_u16(uint16_t x) { return be16toh(x); }

uint16_t letoh_u16(uint16_t x) { return le16toh(x); }

uint32_t betoh_u32(uint32_t x) { return be32toh(x); }

uint32_t letoh_u32(uint32_t x) { return le32toh(x); }

uint64_t betoh_u64(uint64_t x) { return be64toh(x); }

uint64_t letoh_u64(uint64_t x) { return le64toh(x); }

void betoh_f32(float *x) { *(uint32_t*)x = be32toh(*(uint32_t*)x); }

void letoh_f32(float *x) { *(uint32_t*)x = le32toh(*(uint32_t*)x); }

void betoh_f64(double *x) { *(uint64_t*)x = be64toh(*(uint64_t*)x); }

void letoh_f64(double *x) { *(uint64_t*)x = le64toh(*(uint64_t*)x); }
