uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 lightdir;
uniform vec3 eyepos;
uniform vec3 light_pos;
uniform float time;
attribute vec2 in_pos;
attribute vec2 in_baricenter;
attribute vec3 in_color;
attribute vec3 in_center;
attribute float in_scale;
attribute float in_phi;
varying vec4 vertex_color;
varying vec4 vertex_coord;
varying vec4 vertex_norm;
varying vec4 ldir;
varying float intensity;

//float maxf(float a, float b) {
//    if(a < b) return b;
//    else  return a;
//}
#define PI 3.14159265358

float rand(float x) {
    float t = 65526. * sin(x);
    return t - floor(t);
}

void main(){
    //float phi = mod(time * 0.05, 1) * 2 * PI;
    float phi = in_phi;
    float v_sin = sin(phi);
    float v_cos = cos(phi);
    float t1 = time * 0.1;
    t1 = t1 + rand(gl_InstanceID / 100.);
//    float t1 = time + rand(gl_VertexID / 100.);
    t1 = t1 - floor(t1);
    float t2 = t1 * 12.;
    float t3 = t2 * .25 * PI;
    if(t2 > 4.) t3 = 0.;
    float d = abs(sin(t3));
    float k = distance(vec2(in_center.x, in_center.y), vec2(light_pos.x, light_pos.y));
    k = exp(-k * k * 32);
    if(d > 0) {
        d = d;
    } else {
        d = k;
    }
    vec4 in_norm = vec4(0., 0., -1., 0.);
//    vec2 pos = in_pos - d * (in_pos - in_baricenter);
    vec2 pos = d * 4. * in_baricenter + in_pos - d * (in_pos - in_baricenter);
    pos *= in_scale;
    vec4 point = vec4(pos, 0., 1.);
    mat4 local = mat4(1);
    local = local * transpose(mat4(vec4( 1., 0., 0., in_center.x), vec4( 0., 1., 0., in_center.y), vec4(0., 0., 1., 0.), vec4(0., 0., 0., 1.)));
    local = local * mat4(vec4(v_cos, v_sin, 0., 0.), vec4(-v_sin, v_cos, 0., 0.), vec4(0., 0., 1., 0.), vec4(0., 0., 0., 1.));
    gl_Position = projection * local * point;
    gl_Position.z = in_center.z;
    vertex_coord = gl_Position;
//    gl_Position = projection * view * rotation * point;
    vertex_color = vec4(in_color, 1.);
    vertex_color *= (1. - 1./8. + (mod(gl_VertexID, 8.) / 8. / 4. - 1./8.));
    //vertex_color = vec4(abs(2*in_baricenter.x),abs(2*in_baricenter.y),1, 1.);
//    vertex_norm = view * model * in_norm;
    vertex_norm = vec4(0, 0, 1, 0);
    //intensity = maxf(0, dot(ldir, vertex_norm)) + .1;
    intensity = 0.3;
}
