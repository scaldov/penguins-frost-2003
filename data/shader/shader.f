//precision highp float;
uniform float time;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 eyepos;
uniform vec3 light_pos;
varying vec4 vertex_color;
varying vec4 vertex_coord;
varying vec4 vertex_norm;
varying vec4 ldir;
varying float intensity;

float mandelbrot( vec2 c ) {
    vec2 z = vec2(0.0,0.0);
    int i = 0;
    while(i < 30 && (z.x*z.x + z.y*z.y) < 4.0) {
        z = vec2( z.x*z.x-z.y*z.y+c.x, 2.0 * z.x*z.y + c.y );
        i++;
    }
    return float(i);
}

float rand(float x) {
    float t = 65536. * sin(x);
    return t - floor(t);
}


void main() {
    vec4 l = vec4(vertex_coord - vec4(light_pos, 0)*4);
    l = normalize(l);
    vec4 n = normalize(vertex_norm);
    vec4 ydir = vertex_coord - vec4(eyepos, 0);
    float ydist = length(ydir);
    ydir /= ydist;
    vec4 l1 = 2. * n * dot(n, l) - l;
    float lum = dot(l1, ydir) * 1.;
    lum = clamp(lum, 0., 1e10);
    float k = rand(gl_FragCoord.x * gl_FragCoord.y + gl_PrimitiveID);
    k = 0;
    if(k < 0.5){
        gl_FragColor = vertex_color * (.8 + lum * 1);
    } else {
        gl_FragColor = vertex_color;
    }
    gl_FragColor = vertex_color;
}
