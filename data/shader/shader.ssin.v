uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 lightdir;
uniform vec3 eyepos;
uniform float time;
attribute vec2 in_pos;
attribute vec2 in_baricenter;
attribute vec3 in_color;
attribute vec3 in_center;
attribute float in_scale;
attribute float in_phi;
varying vec4 vertex_color;
varying vec4 vertex_pos;
varying vec4 ldir;
varying float intensity;

//float maxf(float a, float b) {
//    if(a < b) return b;
//    else  return a;
//}
#define PI 3.14159265358

void main(){
    //float phi = mod(time * 0.05, 1) * 2 * PI;
    float phi = in_phi;
    float v_sin = sin(phi);
    float v_cos = cos(phi);
    float d = 0.1 + 0.1 * v_sin;
//    d = 0;
    vec4 in_norm = vec4(0., 0., -1., 0.);
    vec2 pos = in_pos - d * (in_pos - in_baricenter);
    pos *= in_scale * v_sin;
    vec4 point = vec4(pos, 0., 1.);
    mat4 local = mat4(1);
    local = local * transpose(mat4(vec4( 1., 0., 0., in_center.x), vec4( 0., 1., 0., in_center.y), vec4(0., 0., 1., 0.), vec4(0., 0., 0., 1.)));
    local = local * mat4(vec4(v_cos, v_sin, 0., 0.), vec4(-v_sin, v_cos, 0., 0.), vec4(0., 0., 1., 0.), vec4(0., 0., 0., 1.));
    gl_Position = projection * local * point;
    gl_Position.z = in_center.z;
//    gl_Position = projection * view * rotation * point;
    vertex_color = vec4(in_color, 1.);
    //vertex_color = vec4(abs(2*in_baricenter.x),abs(2*in_baricenter.y),1, 1.);
    vertex_pos = gl_Position;
    vec4 vertex_norm = view * model * in_norm;
    ldir = view * vec4(lightdir, 0.0);
    //intensity = maxf(0, dot(ldir, vertex_norm)) + .1;
    intensity = max(0.0, dot(ldir, vertex_norm)) + .1;
    intensity = 0.3;
}
