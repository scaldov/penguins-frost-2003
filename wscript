import os
import time

project = 'penguins-frost-2003'
top = '.'
out = 'build'

f_release = 0

def release(ctx):
    global f_release
    f_release = 1


def options(ctx):
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')

def configure(ctx):
    ctx.env.VERSION = os.popen('git rev-parse --short HEAD').read().rstrip()
    ctx.env.TIMESTAMP = str(int(time.time()))
    ctx.env.DEFINES = ['VERSION=0x'+ctx.env.VERSION, 'COMPILE_TIMESTAMP='+ctx.env.TIMESTAMP, 'DEBUG=0']
    print('→ configuring the project')
    ctx.find_program('strip', var='STRIP')
    ctx.find_program('gcc', var='CC')
    ctx.find_program('g++', var='CXX')
    ctx.env.LDFLAGS = '-fopenmp'.split()
    if f_release:
        ctx.env.CFLAGS = '-O1 -fwhole'.split()
    else:
        ctx.env.CFLAGS = '-O1 -g -ggdb -fPIC'.split()
    ctx.env.CFLAGS += '-Wno-unused-variable -Wno-format -Wno-unused-but-set-variable -Wall -fopenmp'.split()
    ctx.env.CXXFLAGS = [] + ctx.env.CFLAGS
    ctx.env.CFLAGS += '-std=gnu99'.split()
    ctx.env.CXXFLAGS += '-std=gnu++2a'.split()
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')

def clean(ctx):
    print('Cleaned.')

def build(ctx):
    source_excl = ['**/list.c', '**/stack.c', '**/mem++.cc', '**/main*.cc']
    source_objlib = ctx.path.ant_glob('code/engine/*.c', excl = source_excl)
    source_objlib += ctx.path.ant_glob('code/engine/*.cc', excl = source_excl)
    source_pf = ctx.path.ant_glob('code/*.cc', excl = source_excl)
    source_pf += ctx.path.ant_glob('code/*.c', excl = source_excl)
    includes = ['.', 'code']
    libs = ['GLEW', 'glfw', 'GL', 'glut', 'pthread']
    print(source_objlib)
    ctx.objects(target='objlib', includes = includes, source = source_objlib)
    ctx.program(target='penguins-frost-2003', source = source_pf + ['code/main.cc'], use=['objlib'], lib=libs, includes = includes)
    ctx.program(target='penguins-frost-2003-shader', source = source_pf + ['code/main_shader.cc'], use=['objlib'], lib=libs, includes = includes)
